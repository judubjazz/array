#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

// ----- //
// Types //
// ----- //

typedef struct {
    int *values;     // Les valeurs dans le tableau
    int currentSize; // Le nombre d'elements dans le tableau
    int capacity;    // Capacite du tableau
} Array;

// ---------- //
// Prototypes //
// ---------- //

/**
 * Cree un tableau dynamique vide et le retourne.
 *
 * @returns  Le tableau
 */
Array Array_create();

/**
 * Insere un element en fin de tableau.
 *
 * Le tableau est redimensionne si sa capacite maximale
 * est atteinte.
 *
 * @param a        Le tableau
 * @param element  L'element a inserer dans le tableau
 */
void Array_insert(Array *a, int element);

/**
 * Supprime l'element en position i et decale les
 * valeurs suivantes d'un indice vers la gauche.
 *
 * @param a  Le tableau
 * @param i  La position de l'element a supprimer
 */
void Array_remove(Array *a, int i);

/**
 * Retourne vrai si et seulement si le tableau contient
 * l'element donne.
 *
 * @param a        Le tableau
 * @param element  L'element a verifier
 * @returns        Vrai si et seulement si l'element est present
 */
bool Array_hasElement(const Array *a, int element);

/**
 * Retourne l'element en position i dans le tableau.
 *
 * @param a  Le tableau
 * @param i  La position de l'element voulu
 * @returns  L'element en position i
 */
int Array_get(const Array *a, int i);

/**
 * Affiche un tableau sur la sortie standard.
 *
 * @param a  Le tableau a afficher
 */
void Array_print(const Array *a);

/**
 * Supprime un tableau dynamique.
 *
 * @param a  Le tableau
 */
void Array_delete(Array *a);

/**
 * Renverse les valeurs d'un tableau.
 *
 * Autrement dit, si les valeurs du tableaux sont `[a1, ..., ak]`, alors le
 * tableau resultant devra etre `[ak, ..., a1|`.
 *
 * Note: Le tableau passe en argument est modifie par cette fonction (on ne
 * fait pas de copie du tableau).
 *
 * @param a  Le tableau
 */
void arrayReverse(Array *a);

// -------------- //
// Implementation //
// -------------- //

Array Array_create() {
    Array a;
    a.values = malloc(sizeof(int));
    a.currentSize = 0;
    a.capacity = 1;
    return a;
}

void Array_insert(Array *a, int element) {
    if (a->currentSize == a->capacity) {
        a->capacity *= 2;
        a->values = realloc(a->values, a->capacity * sizeof(int));
    }
    a->values[a->currentSize] = element;
    ++a->currentSize;
}

void Array_remove(Array *a, int i) {
    if (0 <= i && i < a->currentSize) {
        ++i;
        while (i < a->currentSize) {
            a->values[i - 1] = a->values[i];
            ++i;
        }
        --a->currentSize;
    } else {
        printf("Remove: invalid index %d (size = %d)\n",
               i, a->currentSize);
    }
}

bool Array_hasElement(const Array *a, int element) {
    int i = 0;
    while (i < a->currentSize && Array_get(a, i) != element) {
        ++i;
    }
    return i < a->currentSize;
}

int Array_get(const Array *a, int i) {
    if (0 <= i && i < a->currentSize) {
        return a->values[i];
    } else {
        printf("Get: invalid index %d (size = %d)\n",
               i, a->currentSize);
        return -1;
    }
}

void Array_print(const Array *a) {
    int i;
    printf("[");
    for (i = 0; i < a->currentSize; ++i) {
        printf(" %d", a->values[i]);
    }
    printf(" ]");
}

void Array_delete(Array *a) {
    free(a->values);
}

void Array_reverse(Array *a) {
    // 3 2 7 8 //
    Array  b = Array_create();
    b.values = malloc(a->currentSize * sizeof(int));
    int j =0;

    for (int i = a->currentSize-1; i >= 0; --i){
        b.values[j] = a->values[i];
        ++j;
    }
    for (int i = 0; i < a->currentSize; ++i){
        a->values[i] = b.values[i];
    }

    free(b.values);
}

void Array_reverseTest1(){
    Array a = Array_create();
    printf("Inserting 3, 2, 5, 7, 8, 7\n");
    Array_insert(&a, 3);
    Array_insert(&a, 2);
    Array_insert(&a, 5);
    Array_insert(&a, 7);
    Array_insert(&a, 8);
    Array_insert(&a, 7);
    Array_print(&a);
    Array_reverse(&a);
    Array_print(&a);

    assert(a.values[5]==3);
    assert(a.values[4]==2);
    assert(a.values[3]==5);
    assert(a.values[2]==7);
    assert(a.values[1]==8);
    assert(a.values[0]==7);
    Array_delete(&a);
}

void Array_reverseTest2(){
    Array a = Array_create();
    printf("Inserting 1, 1, 3, 4, 1, 1\n");
    Array_insert(&a, 1);
    Array_insert(&a, 1);
    Array_insert(&a, 3);
    Array_insert(&a, 4);
    Array_insert(&a, 1);
    Array_insert(&a, 1);
    Array_print(&a);
    Array_reverse(&a);
    Array_print(&a);

    assert(a.values[5]==1);
    assert(a.values[4]==1);
    assert(a.values[3]==3);
    assert(a.values[2]==4);
    assert(a.values[1]==1);
    assert(a.values[0]==1);
    Array_delete(&a);
}
// ---- //
// Main //
// ---- //


int main() {
    Array a = Array_create();
    printf("Inserting 3, 2, 5, 7, 8, 7\n");
    Array_insert(&a, 3);
    Array_insert(&a, 2);
    Array_insert(&a, 5);
    Array_insert(&a, 7);
    Array_insert(&a, 8);
    Array_insert(&a, 7);
    Array_print(&a);
    printf("\nRemoving at position 2\n");
    Array_remove(&a, 2);
    Array_print(&a);
    printf("\nRemoving at position 4\n");
    Array_remove(&a, 4);
    Array_print(&a);
    printf("\nRemoving at position 4\n");
    Array_remove(&a, 4);
    Array_print(&a);
    printf("\nHas element %d ? %s\n", 5,
           Array_hasElement(&a, 5) ? "yes" : "no");
    printf("Has element %d ? %s\n", 2,
           Array_hasElement(&a, 2) ? "yes" : "no");

    Array_print(&a);
    Array_reverse(&a);
    Array_print(&a);

    Array_reverseTest1();
    Array_reverseTest2();
    Array_delete(&a);
}
